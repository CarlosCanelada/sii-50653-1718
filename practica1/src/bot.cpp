#pragma once

#include "DatosMemCompartida.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdio.h>
#include <iostream>

int main(void)
{
	DatosMemCompartida *datosm;
	char *org;
	int fdmem;
	
	fdmem=open("/tmp/datosComp.txt", O_RDWR);

	//Proyectamos el fichero abierto en memoria
	org=(char*)mmap(NULL,sizeof(*(datosm)), PROT_WRITE|PROT_READ,MAP_SHARED, fdmem, 0); 
	close(fdmem);
	datosm= (DatosMemCompartida *) org;
	
	//Entramos en el bucle infinito dónde bot decide que hacer
	while (1){		
	
	float posicion_raqueta;
	posicion_raqueta=(datosm->raqueta1.y2+datosm->raqueta1.y1)/2;
	if(posicion_raqueta<datosm->esfera.centro.y)
		datosm->accion=1;
	else if(posicion_raqueta>datosm->esfera.centro.y)
		datosm->accion=-1;
	else
		datosm->accion=0;
	usleep(25000);
	}
	munmap(org,sizeof(*(datosm)));
}

	




